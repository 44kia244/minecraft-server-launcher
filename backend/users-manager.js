const fs = require('fs')
const path = require('path')
const inquirer = require('inquirer')
const { generateHashPassword } = require('./password')

let {
  rootDir: rootDirName
} = require('../config')

const usersJson = path.resolve(rootDirName, 'users.json')

let users = []

function loadUsers () {
  if (!fs.existsSync(usersJson)) {
    fs.writeFileSync(usersJson, '[]\n', { encoding: 'utf-8' })
  }

  try {
    users = JSON.parse(fs.readFileSync(usersJson, { encoding: 'utf-8' }))
  } catch (e) {
    fs.writeFileSync(usersJson, '[]\n', { encoding: 'utf-8' })
    users = []
  }

  return users
}

function saveUsers () {
  fs.writeFileSync(usersJson, JSON.stringify(users, null, '  ') + '\n', { encoding: 'utf-8' })
  return users
}

function addUser (email, password, displayName) {
  const newUser = {
    email: email,
    password: generateHashPassword(password),
    displayName: displayName
  }

  const existingUserIndex = users.findIndex(e => e.email === email)

  if (existingUserIndex >= 0) {
    users.splice(existingUserIndex, 1, newUser)
  } else {
    users.push(newUser)
  }

  saveUsers()

  return users
}

function deleteUser (email) {
  const existingUserIndex = users.findIndex(e => e.email === email)

  if (existingUserIndex >= 0) {
    users.splice(existingUserIndex, 1)
  }

  saveUsers()

  return users
}

async function runner () {
  loadUsers()

  console.log()
  console.log('=== User List ===')
  for (let i = 0; i < users.length; i++) {
    const user = users[i]
    console.log(`${i+1}. ${user.displayName} (${user.email})`)
  }
  if (users.length <= 0) {
    console.log('<No User>')
  }
  console.log()

  const answers = await inquirer.prompt([{
    type: 'list',
    name: 'action',
    message: 'Select Action',
    choices: [
      {
        name: 'Add a User',
        value: 'add',
        short: 'Add a User'
      },
      {
        name: 'Delete a User',
        value: 'delete',
        short: 'Delete a User'
      },
      {
        name: 'Exit Program',
        value: 'exit',
        short: 'Exit Program'
      }
    ]
  }])

  switch (answers.action) {
    case 'add': {
      await runnerAddUser()
      return false
    }
    case 'delete': {
      await runnerDeleteUser()
      return false
    }
    case 'exit': {
      return true;
    }
  }
}

async function runnerAddUser () {
  const answers = await inquirer.prompt([
    {
      type: 'input',
      name: 'email',
      message: 'Enter Email:'
    },
    {
      type: 'input',
      name: 'displayName',
      message: 'Enter Display Name:'
    },
    {
      type: 'password',
      name: 'password',
      message: 'Enter Password:',
      mask: '*'
    },
    {
      type: 'password',
      name: 'confirmPassword',
      message: 'Enter Confirm Password:',
      mask: '*'
    }
  ])

  if (!answers.email) {
    console.log('Email cannot be empty')
    return
  }

  if (!answers.displayName) {
    console.log('Display Name cannot be empty')
    return
  }

  if (!answers.password) {
    console.log('Password cannot be empty')
    return
  }

  if (answers.password !== answers.confirmPassword) {
    console.log('Password do not match')
    return
  }

  addUser(answers.email, answers.password, answers.displayName)
}

async function runnerDeleteUser () {
  const answers = await inquirer.prompt([{
    type: 'list',
    name: 'email',
    message: 'Select a User to Delete',
    choices: users.map(u => ({
      name: `${u.displayName} (${u.email})`,
      value: u.email,
      short: u.email
    })).concat([{
      name: 'Cancel',
      value: '',
      short: 'Cancel'
    }])
  }])

  if (!answers.email) return
  deleteUser(answers.email)
}

async function main () {
  let exit = false

  while (!exit) {
    exit = await runner()
  }

  process.exit(0)
}

main()
