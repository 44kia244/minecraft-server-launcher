const cryptoRandomString = require('crypto-random-string')
const crypto = require('crypto')

exports.generateHashPassword = function (password) {
  const salt = cryptoRandomString({ length: 10, characters: '!"#%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~' })
  const passwordWithSalt = `${salt}${password}`
  const passwordHash = crypto.createHash('sha256').update(passwordWithSalt).digest('hex')

  return `${salt}$${passwordHash}`
}

exports.checkPassword = function (correctPasswordHash, password) {
  // get salt from correct password hash
  const salt = correctPasswordHash.split('$')[0]

  // hash the input password with the same salt
  const passwordWithSalt = `${salt}${password}`
  const passwordHash = crypto.createHash('sha256').update(passwordWithSalt).digest('hex')
  const passwordHashWithSalt = `${salt}$${passwordHash}`

  // the result must be the same
  return passwordHashWithSalt === correctPasswordHash
}
