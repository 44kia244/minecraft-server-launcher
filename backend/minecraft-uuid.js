const crypto = require('crypto')

module.exports = function generateOfflineUUID (playerName, prefix = 'OfflinePlayer') {
  let data = crypto.createHash('md5').update(`${prefix}:${playerName}`).digest('hex').match(/[0-9a-f]{2}/ig).map(e => parseInt(e, 16))

  data[6] = data[6] & 0x0f | 0x30
  data[8] = data[8] & 0x3f | 0x80

  data = data.map(e => {
    let hex = e.toString(16)
    while (hex.length < 2) hex = '0' + hex
    return hex
  }).join('')

  return `${data.substr(0, 8)}-${data.substr(8, 4)}-${data.substr(12, 4)}-${data.substr(16, 4)}-${data.substr(20)}`
}
