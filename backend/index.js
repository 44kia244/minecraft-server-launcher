const path = require('path')
const fs = require('fs')
const url = require('url')
const { spawn } = require('child_process')
const express = require('express')
const http = require('http')
const WebSocket = require('ws')
const stream = require('stream')
const generateOfflineUUID = require('./minecraft-uuid')
const jwt = require('jsonwebtoken')
const bodyParser = require('body-parser')
const devNull = require('dev-null')
const { checkPassword } = require('./password')

const app = express()
const httpServer = http.createServer(app)
const consoleWS = new WebSocket.Server({ noServer: true })
let serverProcess = null
let users = null

// get config from config file
let {
    rootDir: rootDirName,
    javaExecutable: java,
    serverAutoStart,
    serverJar: server,
    minecraftCWD,
    maxWebSocketConnections: MAX_WS_LISTENERS,
    jwtSecret,
    javaArgs,
    minecraftArgs,
    serverPort,
    uuidGenerationPrefix,
} = require('../config')

// define constant config
let frontendDir = 'frontend/dist'
let whitelistJson = 'whitelist.json'
let whitelistTxt = 'white-list.txt'
let usersJson = 'users.json'

// process relative path in config to absolute path
server = path.resolve(rootDirName, server)
minecraftCWD = path.resolve(rootDirName, minecraftCWD)
frontendDir = path.resolve(rootDirName, frontendDir)
whitelistJson = path.resolve(minecraftCWD, whitelistJson)
whitelistTxt = path.resolve(minecraftCWD, whitelistTxt)
usersJson = path.resolve(rootDirName, usersJson)

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use((req, res, next) => {
    req.user = null

    if (req.headers['authorization']) {
        let token = req.headers['authorization']
        if (token.startsWith('Bearer ')) token = token.split(' ')[1]

        try {
            req.user = jwt.verify(token, jwtSecret)
        } catch (e) {
        }
    }

    next()
})

const requireAuth = (req, res, next) => {
    if (!req.user) {
        return res.status(401).json({
            error: 'Unauthorized'
        })
    }

    next()
}

const requireGuest = (req, res, next) => {
    if (req.user) {
        return res.status(401).json({
            error: 'Unauthorized'
        })
    }

    next()
}

// proxy stream from web to server
const serverStdinStream = new stream.PassThrough({
    encoding: 'utf-8',
    emitClose: false,
    autoClose: false
})

// proxy stream from server to web
const serverStdoutStream = new stream.PassThrough({
    encoding: 'utf-8',
    emitClose: false,
    autoClose: false
})

// to let the stream flow even is no client is online
serverStdoutStream.pipe(devNull())

function loadUsers () {
    if (!fs.existsSync(usersJson)) {
        fs.writeFileSync(usersJson, '[]\n', { encoding: 'utf-8' })
    }

    try {
        users = JSON.parse(fs.readFileSync(usersJson, { encoding: 'utf-8' }))
    } catch (e) {
        fs.writeFileSync(usersJson, '[]\n', { encoding: 'utf-8' })
        users = []
    }

    return users
}

function connectClient (clientStream) {
    clientStream.pipe(serverStdinStream)
    serverStdoutStream.pipe(clientStream)
}

function disconnectClient (clientStream) {
    clientStream.unpipe(serverStdinStream)
    serverStdoutStream.unpipe(clientStream)
}

function connectServer () {
    if (serverProcess) {
        serverStdinStream.pipe(serverProcess.stdin, { end: false })
        serverProcess.stdout.pipe(serverStdoutStream, { end: false })
        serverProcess.stderr.pipe(serverStdoutStream, { end: false })
    }
}

function disconnectServer () {
    if (serverProcess) {
        serverStdinStream.unpipe(serverProcess.stdin)
        serverProcess.stdout.unpipe(serverStdoutStream)
        serverProcess.stderr.unpipe(serverStdoutStream)
    }
}

function startServer () {
    if (serverProcess) return
    serverProcess = spawn(
        java,
        [ ...javaArgs, '-jar', server, ...minecraftArgs ],
        {
            cwd: minecraftCWD,
            stdio: 'pipe'
        }
    )

    connectServer()

    serverProcess.on('exit', (code, signal) => {
        disconnectServer()
        console.log(`server exited with code ${code} (signal = ${signal || null})`)
        serverProcess = null
    })
}

function stopServer () {
    if (!serverProcess) return false
    serverProcess.stdin.write('stop\n')
    return true
}

function reloadServer () {
    if (!serverProcess) return false
    serverProcess.stdin.write('reload\n')
    return true
}

function reloadServerWhitelist () {
    if (!serverProcess) return false
    serverProcess.stdin.write('whitelist reload\n')
    return true
}

function killServer () {
    if (!serverProcess) return false
    const success = serverProcess.kill('SIGINT')
    if (success) serverProcess = null
    return success
}

function checkWhitelistFile () {
    if (!fs.existsSync(whitelistJson)) saveWhitelist()
}

function getWhitelist () {
    try {
        checkWhitelistFile()
        return JSON.parse(fs.readFileSync(whitelistJson, { encoding: 'utf-8' }))
    } catch (e) {
        return []
    }
}

function saveWhitelist (whitelist = []) {
    try {
		fs.writeFileSync(whitelistJson, JSON.stringify(whitelist, null, '  ') + '\n', {
			encoding: 'utf-8'
		})
		
        if (!newWhitelistFormatOnly) {
			fs.writeFileSync(whitelistTxt, whitelist.map(e => e.name).join('\n'), {
				encoding: 'utf-8'
			})
		}

        return whitelist
    } catch (e) {
        return []
    }
}

function addToWhitelist (name) {
    try {
        checkWhitelistFile()
        const whitelist = getWhitelist()

        if (!whitelist.find(e => e.name === name)) {
            whitelist.push({
                name,
                uuid: generateOfflineUUID(name, uuidGenerationPrefix)
            })
            saveWhitelist(whitelist)
            reloadServerWhitelist()
        }

        return whitelist
    } catch (e) {
        console.error(e)
        return []
    }
}

function deleteFromWhitelist (deleteUUID) {
    try {
        checkWhitelistFile()
        const whitelist = getWhitelist()
        const deleteIndex = whitelist.findIndex(e => e.uuid === deleteUUID)

        if (deleteIndex >= 0) {
            whitelist.splice(deleteIndex, 1)
            saveWhitelist(whitelist)
            reloadServerWhitelist()
        }

        return whitelist
    } catch (e) {
        return []
    }
}

app.post('/api/login', requireGuest, (req, res) => {
    const {
        email, password
    } = req.body

    let jwtToken = ''

    loadUsers()
    const user = users.find(e => e.email === email)

    if (user && checkPassword(user.password, password)) {
        jwtToken = jwt.sign({
            email: user.email,
            displayName: user.displayName
        }, jwtSecret)
    }

    if (jwtToken) {
        return res.status(200).json({
            token: jwtToken
        })
    } else {
        return res.status(401).json({
            error: 'Invalid email or password'
        })
    }
})

app.get('/api/me', requireAuth, (req, res) => {
    res.status(200).json({
        user: req.user
    })
})

app.get('/api/status', requireAuth, (req, res) => {
    res.status(200).json({
        serverStarted: !!serverProcess
    })
})

app.post('/api/start', requireAuth, (req, res) => {
    if (!serverProcess) startServer()
    res.status(200).json({
        serverStarted: !!serverProcess
    })
})

app.post('/api/stop', requireAuth, (req, res) => {
    if (serverProcess) stopServer()
    res.status(200).json({
        serverStarted: !!serverProcess
    })
})

app.post('/api/reload', requireAuth, (req, res) => {
    if (serverProcess) reloadServer()
    res.status(200).json({
        serverStarted: !!serverProcess
    })
})

app.post('/api/kill', requireAuth, (req, res) => {
    if (serverProcess) killServer()
    res.status(200).json({
        serverStarted: !!serverProcess
    })
})

app.get('/api/whitelist', requireAuth, (req, res) => {
    res.status(200).json({
        whitelist: getWhitelist()
    })
})

app.post('/api/whitelist/:username', requireAuth, (req, res) => {
    const username = req.params.username || null

    if (!username) return res.status(400).json({
        error: 'username is required'
    })

    res.status(200).json({
        whitelist: addToWhitelist(username)
    })
})

app.delete('/api/whitelist/:uuid', requireAuth, (req, res) => {
    const deleteUUID = req.params.uuid || null

    if (!deleteUUID) return res.status(400).json({
        error: 'uuid is required'
    })

    res.status(200).json({
        whitelist: deleteFromWhitelist(deleteUUID)
    })
})

app.all('/api/*', (req, res) => {
    res.status(404).json({
        error: 'Not Found'
    })
})

app.use(express.static(frontendDir))

app.all('*', (req, res) => {
    res.sendFile(path.join(frontendDir, 'index.html'))
})

consoleWS.on('connection', (ws) => {
    ws.isAlive = true
    ws.on('pong', function() { this.isAlive = true })

    const duplexStream = WebSocket.createWebSocketStream(ws, { encoding: 'utf8', end: false })

    ws.on('close', () => disconnectClient(duplexStream))
    duplexStream.on('error', () => disconnectClient(duplexStream))

    connectClient(duplexStream)
})

const aliveCheckInterval = setInterval(() => {
    consoleWS.clients.forEach((wsClient) => {
        if (wsClient.isAlive === false) return wsClient.terminate()

        wsClient.isAlive = false
        wsClient.ping('are you alive')
    })
}, 1000)

httpServer.on('upgrade', (request, socket, head) => {
    const urlParts = new url.URL(request.url, 'http://dummy')
    const pathname = urlParts.pathname
    const token = urlParts.searchParams.get('authorization') || ''

    switch(pathname) {
        case '/api/console': {
            try {
                jwt.verify(token, jwtSecret)

                if (consoleWS.clients.length >= MAX_WS_LISTENERS) {
                    socket.destroy()
                    break
                }

                consoleWS.handleUpgrade(request, socket, head, function done(ws) {
                    consoleWS.emit('connection', ws, request)
                })

                break
            } catch (e) {
                socket.destroy()
                break
            }
        }
        default: {
            socket.destroy()
            break
        }
    }
})


httpServer.listen(serverPort, () => {
    console.log(`Listening on port ${serverPort}`)

    if (serverAutoStart) {
        console.log('Auto-Starting Server...')
        startServer()
    }
})
