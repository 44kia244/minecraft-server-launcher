module.exports = {
    rootDir: __dirname,
    javaExecutable: 'C:\\Program Files\\AdoptOpenJDK\\jdk-8.0.282.8-hotspot\\bin\\java.exe',
    serverAutoStart: false,
    serverJar: 'minecraft.jar',
    minecraftCWD: 'minecraft-data',
    maxWebSocketConnections: 10,
    jwtSecret: 'SOMETHING-SECRET-HERE',
    javaArgs: [
        '-Xms1G',
        '-Xmx2G',
        '-d64'
    ],
    minecraftArgs: [],
    serverPort: 80,
    uuidGenerationPrefix: 'OfflinePlayer',
	newWhitelistFormatOnly: true
}
