import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    consoleWS: null,
    consoleBuffer: [],
    consoleReader: new FileReader(),
    consoleReaderQueue: [],
    consoleReaderRunning: false,
    isServerRunning: false,
    whitelist: [],
    jwtToken: '',
    currentUser: null,
    serverStatusIntervalRef: null
  },
  getters: {
    serverState (state) {
      return state.isServerRunning ? 'RUNNING' : 'STOPPED'
    }
  },
  mutations: {
    connectToConsoleWS (state) {
      if (state.consoleWS) {
        try {
          state.consoleWS.close()
          state.consoleWS = null
        } catch (e) {
        }
      }

      const location = window.location
      let hostname = location.hostname
      let query = ''
      if (location.port) hostname += `:${location.port}`
      if (location.protocol === 'http:') hostname = `ws://${hostname}`
      if (location.protocol === 'https:') hostname = `wss://${hostname}`
      if (state.jwtToken) query += `authorization=${state.jwtToken}`
      state.consoleWS = new WebSocket(hostname + '/api/console?' + query)
    },
    disconnectFromConsoleWS (state) {
      if (state.consoleWS) {
        state.consoleWS.close()
        state.consoleWS = null
      }
    },
    setConsoleReaderRunning (state, isRunning) {
      state.consoleReaderRunning = !!isRunning
    },
    addConsoleReaderQueue (state, blob) {
      state.consoleReaderQueue.push(blob)
    },
    addToConsoleBuffer (state, text) {
      state.consoleBuffer.push(text)
    },
    setServerRunning (state, isServerRunning) {
      state.isServerRunning = !!isServerRunning
    },
    setWhitelist (state, whitelist) {
      state.whitelist = whitelist
    },
    setJwtToken (state, jwtToken) {
      state.jwtToken = jwtToken
      if (jwtToken) localStorage.setItem('token', jwtToken)
      else localStorage.removeItem('token')
    },
    setCurrentUser (state, currentUser) {
      state.currentUser = currentUser
    },
    setServerStatusIntervalRef (state, serverStatusIntervalRef) {
      state.serverStatusIntervalRef = serverStatusIntervalRef
    }
  },
  actions: {
    boot (store) {
      const savedJwtToken = localStorage.getItem('token')
      if (savedJwtToken) store.commit('setJwtToken', savedJwtToken)

      axios.interceptors.request.use((config) => {
        const token = store.state.jwtToken

        if (token) {
          config.headers.Authorization = token
        }

        return config
      })

      store.dispatch('loadCurrentUser')

      store.state.consoleReader.addEventListener('loadend', (event) => {
        store.dispatch('onConsoleReaderLoaded', event)
      })
    },
    async loadCurrentUser (store) {
      try {
        const response = await axios.get('/api/me')
        store.commit('setCurrentUser', response.data.user)
      } catch (e) {
        store.commit('setCurrentUser', null)
      }
    },
    setupConsole (store) {
      store.commit('connectToConsoleWS')

      store.state.consoleWS.addEventListener('message', function (event) {
        if (event.data instanceof Blob) {
          store.dispatch('onConsoleMessage', event.data)
        }
      })
    },
    disconnectConsole (store) {
      store.commit('disconnectFromConsoleWS')
    },
    sendToWebSocket (store, message) {
      if (!store.state.consoleWS) return
      store.state.consoleWS.send(message)
    },
    onConsoleMessage (store, blob) {
      store.commit('addConsoleReaderQueue', blob)

      if (!store.state.consoleReaderRunning) {
        store.commit('setConsoleReaderRunning', true)
        store.state.consoleReader.readAsText(store.state.consoleReaderQueue.shift())
      }
    },
    onConsoleReaderLoaded (store, event) {
      const text = event.target.result
      store.commit('addToConsoleBuffer', text)

      if (store.state.consoleReaderQueue.length > 0) {
        store.state.consoleReader.readAsText(store.state.consoleReaderQueue.shift())
      } else {
        store.commit('setConsoleReaderRunning', false)
      }
    },
    async updateServerStatus (store) {
      const response = await axios.get('/api/status')
      store.commit('setServerRunning', response.data.serverStarted)
    },
    async startServerStatusPolling (store) {
      if (store.state.serverStatusIntervalRef === null) {
        await store.dispatch('updateServerStatus')

        const intervalRef = setInterval(async () => {
          await store.dispatch('updateServerStatus')
        }, 1000)

        store.commit('setServerStatusIntervalRef', intervalRef)
      }
    },
    async clearServerStatusPolling (store) {
      if (store.state.serverStatusIntervalRef) {
        clearInterval(store.state.serverStatusIntervalRef)
        store.commit('setServerStatusIntervalRef', null)
      }
    },
    async startServer (store) {
      const response = await axios.post('/api/start')
      store.commit('setServerRunning', response.data.serverStarted)
    },
    async stopServer (store) {
      const response = await axios.post('/api/stop')
      store.commit('setServerRunning', response.data.serverStarted)
    },
    async reloadServer (store) {
      const response = await axios.post('/api/reload')
      store.commit('setServerRunning', response.data.serverStarted)
    },
    async killServer (store) {
      const response = await axios.post('/api/kill')
      store.commit('setServerRunning', response.data.serverStarted)
    },
    async loadWhitelist (store) {
      const response = await axios.get('/api/whitelist')
      store.commit('setWhitelist', response.data.whitelist)
    },
    async addToWhitelist (store, username) {
      const response = await axios.post(`/api/whitelist/${username}`)
      store.commit('setWhitelist', response.data.whitelist)
    },
    async removeFromWhitelist (store, uuid) {
      const response = await axios.delete(`/api/whitelist/${uuid}`)
      store.commit('setWhitelist', response.data.whitelist)
    },
    async login (store, { email, password }) {
      const response = await axios.post('/api/login', {
        email, password
      })
      store.commit('setJwtToken', response.data.token)
      store.dispatch('loadCurrentUser')
    },
    async logout (store) {
      store.commit('setJwtToken', '')
      store.commit('setCurrentUser', null)
    }
  },
  modules: {
  }
})
