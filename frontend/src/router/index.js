import Vue from 'vue'
import VueRouter from 'vue-router'
import Console from '../views/Console.vue'
import Whitelist from '../views/Whitelist.vue'
import Login from '@/views/Login'
import Layout from '@/Layout'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: Layout,
    children: [
      {
        path: '/',
        name: 'Login',
        component: Login
      },
      {
        path: '/console',
        name: 'Console',
        component: Console
      },
      {
        path: '/whitelist-management',
        name: 'Whitelist',
        component: Whitelist
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
